# CarCar

An application for managing aspects of an automobile
dealership—specifically its inventory, service center, and sales.

Team:

* Mel Kazaka - Services
* Jose Davila - Sales

## How to Run this App
 - Fork this repository
 - clone forked repository:
    - git clone <<repo.url.here>>
 - Ensure Docker in running before running these commands:
    - docker volume create beta-data
    - docker-compose build
    - docker-compose up
- Verify all created Docker containers  running
- View project in browser: http://localhost:3000/

## Diagram
                                +------------------------------------------+
                                |              Front-End (React)           |
                                |                                          |
                                +------------------------------------------+
                                             |
                   +-------------------------|-------------------------+
                   |                         |                         |
+------------------+---------------+     +------------------+      +---------------------+
|      Inventory Microservice      |    |Service Microservice |    | Sales Microservice  |
|                                  |    |                     |    |                     |
|  +---------------------------+   |    |  +----------------+ |    |  +---------------+  |
|  |                           |   |    |  |                | |    |  |               |  |
|  |   Manufacturer Entity     |<--+----+--|  Technician    | |    |  |Sales People   |  |
|  |                           |   |    |  |  Entity        | |    |  |   Entity      |  |
|  +---------------------------+   |    |  +----------------+ |    |  +---------------+  |
|  |                           |   |    |  |                | |    |  |               |  |
|  |   VehicleModel Entity     |<--+----+-->|  Appointment  | |    |  |   Sales       |  |
|  |                           |   |    |  |  Entity        | |    |  |   Entity      |  |
|  +---------------------------+   |    |  +----------------+ |    |  +---------------+  |
|  |                           |   |    |                     |    |                     |
|  |   Automobile Entity       |<--+----+---------------------+    +---------------------+
|  |                           |   |
|  +---------------------------+   |
|                                  |
+----------------------------------+


## API Documentation
- Inventory Microservice:
    This handles creating Vehicles(Manufacturer, Model, Automobile)
- Service Microservice:
    This handles creating Technicians and Appointments. It needs to reference
        Inventory Microservice for its Automobile Model(ex; use of AutomobileVO)
        to utilize "vin" and "sold". This will help determine VIP treatment if
        the was vehicle from sold CarCar inventory.
- Sales Microservice:
    This handles creating Sales People and Sales. It needs to reference
        Inventory Microservice for its Automobile Model(ex; use AutomobileVO)
        for conducting sales.
### URLs and Ports
- Inventory Microservice:
    Base URL: http://localhost:8100
    Endpoints:
    List Manufacturers: GET /api/manufacturers/
    Create Manufacturer: POST /api/manufacturers/
    Get Specific Manufacturer: GET /api/manufacturers/:id/
    Update Specific Manufacturer: PUT /api/manufacturers/:id/
    Delete Specific Manufacturer: DELETE /api/manufacturers/:id/
    List Vehicle Models: GET /api/models/
    Create Vehicle Model: POST /api/models/
    Get Specific Vehicle Model: GET /api/models/:id/
    Update Specific Vehicle Model: PUT /api/models/:id/
    Delete Specific Vehicle Model: DELETE /api/models/:id/
    List Automobiles: GET /api/automobiles/
    Create Automobile: POST /api/automobiles/
    Get Specific Automobile: GET /api/automobiles/:vin/
    Update Specific Automobile: PUT /api/automobiles/:vin/
    Delete Specific Automobile: DELETE /api/automobiles/:vin/

- Service Microservice:
    Base URL: http://localhost:8080
    Endpoints:
    List Technicians: GET /api/technicians/
    Create Technician: POST /api/technicians/
    Delete Specific Technician: DELETE /api/technicians/:id/
    List Appointments: GET /api/appointments/
    Create Appointment: POST /api/appointments/
    Delete Specific Appointment: DELETE /api/appointments/:id/
    Cancel Appointment: PUT /api/appointments/:id/cancel/
    Finish Appointment: PUT /api/appointments/:id/finish/

- Sales Microservice:
    Base URL: http://localhost:8090
    Endpoints:
    List Salespeople: GET /api/salespeople/
    Create Salesperson: POST /api/salespeople/
    Delete Specific Salesperson: DELETE /api/salespeople/:id/
    List Customers: GET /api/customers/
    Create Customer: POST /api/customers/
    Delete Specific Customer: DELETE /api/customers/:id/
    List Sales: GET /api/sales/
    Create Sale: POST /api/sales/
    Delete Sale: DELETE /api/sales/:id


### Inventory API (Optional)
- This API handles teh creation of vehicles, starting from Manufacturer => Vehicle Model => Automobile, while also
    maintaining a list of all creations.

    ManufacturerForm example: http://localhost:8100/api/manufacturers/add/
{
  "name": "Honda"
}

    ModelsForm example: http://localhost:8100/api/models/new/
{
  "name": "Accord",
  "picture_url": "<<<insert picture url>>>",
  "manufacturer_id": 1
}

    AutomobilesForm example: http://localhost:3000/automobiles/new
{
  "color": "red",
  "year": 2024,
  "vin": "1B3BBFB2AN120244",
  "model_id": 1
}
    Feature = The "VIN" form field will always output uppercase as that is the norm for VINS.

### Service API
 - This API handles creating and tracking of Technicians and Appointments. It also maintains a Service History.
    TechnicianForm/AppointmentForm are used to create and TechnicianList/AppointmentList will provide a list.
    JSON body is used to send data form all endpoints:
    TechnicianForm example: http://localhost:3000/technician/add
{
	"employee_id": "jdoe",
	"first_name": "John",
	"last_name": "Doe"
}
    Feature = The employee_id field will auto-populate with first name initial and full last name, all lowercase.
                This feature is for convenience and is optional.

    AppointmentForm example: http://localhost:3000/appointments/new
{
	"customer": "Prince Vegeta",
    "vin": "1B3BBFB2AN120244",
    "date_time": "2024-02-14",
	"technician": 1,
    "reason": "Air Filter",
	"status": "pending"
}
    Feature = The "VIN" form field will always output uppercase as that is the norm for VINS.

    AppointmentList:
    Feature = VIP status is built in by tracking if the VIN matches a sold Automobile from CarCar inventory. If the customer is VIP it will be displayed on the screen with "Yes", otherwise it would be "No".
    Also, from this page, the user has access to buttons that can either Finish or Cancel the appointment. Once pressed,
    a message displays of successful update and the appointment is removed from the List.

    ServiceHistory: http://localhost:3000/appointments/history
    This maintains displays all services rendered by CarCar. Services rendered is searchable by VIN for specific inquiries.

### Sales API
How to Start Your Project
Before You Start:
You need Node.js on your computer. It's like the engine that runs your code.
Getting the Code:
First, you need to get the project code onto your computer. You do this by "cloning" the code from a place called GitHub. It's like downloading, but for code.
Use this command in your computer's terminal (a place where you type commands to your computer): git clone https://github.com/yourusername/yourprojectname.git. This command says, "Hey computer, go grab the project from GitHub."
Setting Things Up:
Move into your project's folder by typing cd yourprojectname in the terminal. Think of it as walking into the project's room.
Inside, you need to set up the project by running npm install. This tells your computer to grab all the tools it needs to run the project.
Making It Yours:
Projects often have secret keys or settings. You usually copy a file named .env.example to a new file called .env and fill in your own settings.
Preparing the Database:
There might be instructions on setting up your database, like organizing shelves before you start using them.
Starting the Project:
Finally, you start the project with npm start. It's like turning on a car; now it's running and ready.
By default, you can see your project working in your web browser by going to http://localhost:3000.
Understanding the Project's Layout
Imagine a diagram showing how everything in your project connects:
Your project is like a shop. The React application is the front part where customers walk in. It runs in the user's browser.
The Backend API is like the storage room. It's on a computer somewhere (could be yours or a cloud server) and does the heavy lifting, like fetching data.
The Database is where all the information is stored, like a warehouse for your data.
How Everything Talks to Each Other
Your shop's front (React app) talks to the storage room (Backend API) at http://localhost:8090.
The storage room knows how to talk to the warehouse (Database) to get or store information.
How to Use the Storage Room (CRUD Operations)
For each part of your project (like salespeople or sales), there are ways to:
Create new records (like adding a new salesperson).
Read records (seeing what salespeople are there).
Update records (changing details about a salesperson).
Delete records (removing a salesperson).
Special Items in Your Project (Value Objects)
Some things in your project are just used to describe stuff and don't need to be tracked individually, like a "Car" in a sale. It has details (make, model, VIN), but you're not tracking each car in the world, just using the info to describe sales.

## Value Objects
 - AutomobileVO, Manufacturer, VehicleModel, and Automobile, Technician, Customer, and Sale
