from django.urls import path

from .api_views import (
    api_technician_list,
    api_appointment_list,
    api_appointment_details,
    api_technician_details,
    api_appointment_finish,
    api_appointment_cancel,
)

urlpatterns = [
    path(
        "technicians/",
         api_technician_list,
         name="api_technician_list",
    ),
    path(
        "technicians/<int:pk>/",
        api_technician_details,
        name="api_technician_details",
    ),
    path(
        "appointments/",
        api_appointment_list,
        name="api_appointment_list",
    ),
    path(
        "appointments/<int:pk>/",
        api_appointment_details,
        name="api_appointment_details",
    ),
    path(
        "appointments/<int:pk>/finish/",
        api_appointment_finish,
        name="api_appointment_finish",
    ),
    path(
        "appointments/<int:pk>/cancel/",
        api_appointment_cancel,
        name="api_appointment_cancel",
    ),
]