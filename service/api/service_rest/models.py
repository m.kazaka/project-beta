from django.db import models

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=100, unique=True)
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

class Technician(models.Model):
    first_name = models.CharField(max_length=100, null=True)
    last_name = models.CharField(max_length=100, null=True)
    employee_id = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

class AppointmentStatus(models.Model):
    STATUS_CHOICES = (
        ('pending', 'Pending'),
        ('finished', 'Finished'),
        ('cancelled', 'Cancelled'),
    )

    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default='pending')

    def __str__(self):
        return self.status

class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=100)
    customer = models.CharField(max_length=100,null=True)
    vin = models.CharField(max_length=20)
    technician = models.ForeignKey(
        Technician,
        null=True,
        related_name = "technician",
        on_delete=models.CASCADE,
    )
    status = models.ForeignKey (
        AppointmentStatus,
        on_delete = models.CASCADE,
    )
    def __str__(self):
        return f"{self.date_time} - {self.reason} ({self.status})"
