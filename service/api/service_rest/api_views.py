from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment, AppointmentStatus

class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "vin",
        "sold",
    ]

class AppointmentStatusEncoder(ModelEncoder):
    model = AppointmentStatus
    properties = (
        "status",
    )

class TechnicianEncoder(ModelEncoder):
    model= Technician
    properties = [
        "id",
        "employee_id",
        "first_name",
        "last_name",
    ]

class AppointmentEncoder(ModelEncoder):
    model= Appointment
    properties = [
        "id",
        "customer",
        "vin",
        "date_time",
        "technician",
        "reason",
        "status",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
        "status": AppointmentStatusEncoder(),
    }



@require_http_methods(["GET", "POST"])
def api_technician_list(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create technician"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE"])
def api_technician_details(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                {"technician": technician},
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            Technician.objects.get(id=pk)

            props = ["first_name", "last_name", "employee_id"]
            for prop in props:
                if prop in content:
                    setattr(technician, prop, content[prop])
            technician.save()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET", "POST"])
def api_appointment_list(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            print(f"Received content: {content}")
            technician_id = content["technician"]
            technician = Technician.objects.get(id=technician_id)
            content["technician"] = technician

            status = content.get("status", "pending")
            status_instance, created = AppointmentStatus.objects.get_or_create(status=status)
            content["status"] = status_instance

            appointment = Appointment.objects.create(**content)

            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
            )
        except Exception as e:
            print(f"Error creating appointment. Exception type: {type(e).__name__}. Details: {e}")
            response = JsonResponse(
                {"message": "Could not create appointment"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE", "PUT"])
def api_appointment_details(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            appointment = Appointment.objects.get(id=pk)
            props = ["status"]
            for prop in props:
                if prop in content:
                    setattr(appointment, prop, content[prop])
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["PUT"])
def api_appointment_finish(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        if appointment.status.status == 'pending':
            finished_status, _ = AppointmentStatus.objects.get_or_create(status='finished')
            appointment.status = finished_status
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        else:
            response = JsonResponse({"message": "Appointment status in is not 'pending'"})
            response.status_code = 400
            return response

    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "Appointment does not exist"})
        response.status_code = 404
        return response

@require_http_methods(["PUT"])
def api_appointment_cancel(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        if appointment.status.status == 'pending':
            finished_status, _ = AppointmentStatus.objects.get_or_create(status='cancelled')
            appointment.status = finished_status
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        else:
            response = JsonResponse({"message": "Appointment status in is not 'pending'"})
            response.status_code = 400
            return response
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "Appointment does not exist"})
        response.status_code = 404
        return response