import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

function SalespersonForm() {
  const [formData, setFormData] = useState({
    first_name: '',
    last_name: '',
    employee_id: '',
  });

  const [salespeople, setSalespeople] = useState([]);
  const navigate = useNavigate();

  const handleSubmit = async (event) => {
    event.preventDefault();
    const url = 'http://localhost:8090/api/salespeople/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        const newSalesperson = await response.json();
        setSalespeople([...salespeople, newSalesperson]);
        setFormData({
          first_name: '',
          last_name: '',
          employee_id: '',
        });

        navigate('/salespeople/list');
      } else {
        throw new Error('Failed to create salesperson');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  const handleFormChange = (e) => {
    const inputName = e.target.name;
    const value = e.target.value;

    setFormData((prevState) => ({
      ...prevState,
      [inputName]: value,
    }));
  };

  const handleDelete = async (salespersonId) => {
    const url = `http://localhost:8090/api/salespeople/${salespersonId}`;
    const fetchConfig = {
      method: 'delete',
    };

    try {
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        const updatedSalespeople = salespeople.filter((salesperson) => salesperson.employee_id !== salespersonId);
        setSalespeople(updatedSalespeople);
      } else {
        throw new Error('Failed to delete salesperson');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  const handleUpdate = async (salespersonId) => {
    const url = `http://localhost:8090/api/salespeople/${salespersonId}`;
    const fetchConfig = {
      method: 'put',
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const response = await fetch(url, fetchConfig);
      if (response.ok) {

      } else {
        throw new Error('Failed to update salesperson');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <>
      <div>
        <h1>Add a Salesperson</h1>
        <form onSubmit={handleSubmit} id="add-salesperson-form">
          <div className="mb-3">
            <input onChange={handleFormChange}
              value={formData.first_name}
              placeholder="First name..."
              required type="text"
              name="first_name"
              autoComplete="off"
              className="form-control" />
          </div>
          <div className="mb-3">
            <input onChange={handleFormChange}
              value={formData.last_name}
              placeholder="Last name..."
              required type="text"
              name="last_name"
              autoComplete="off"
              className="form-control" />
          </div>
          <div className="mb-3">
            <input onChange={handleFormChange}
              value={formData.employee_id}
              placeholder="Employee ID..."
              required type="text"
              name="employee_id"
              autoComplete="off"
              className="form-control" />
          </div>
          <button type="submit" className="btn btn-primary" id="style-2">
            <span>Create</span>
          </button>
        </form>
        <ul>
          {salespeople.map(salesperson => (
            <li key={salesperson.employee_id}>
              {salesperson.first_name} {salesperson.last_name}
              <button onClick={() => handleDelete(salesperson.employee_id)}>Delete</button>
              <button onClick={() => handleUpdate(salesperson.employee_id)}>Update</button>
            </li>
          ))}
        </ul>
      </div>
    </>
  );
}

export default SalespersonForm;
