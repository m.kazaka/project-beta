import React, {  useEffect, useState } from "react";

function ManufacturerList () {
    const[manufacturers, setManufacturers] = useState([])

    const getData = async () => {
        try {
            const response = await fetch('http://localhost:8100/api/manufacturers/');
            if (response.ok) {
                const data = await response.json();
                setManufacturers(data.manufacturers);
            } else {
                throw new Error('Failed to fetch data');
            }
        } catch (error) {
            console.error('error fetching manufacturer data', error);
        }
    };
    useEffect(() =>{
        getData()
    }, [])
    return (
        <>
        <div>
            <h1>Manufacturers</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map(manufacturer => {
                        return (
                            <tr key={ manufacturer.id }>
                                <td>{ manufacturer.name }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
        </>
    )
}

export default ManufacturerList;