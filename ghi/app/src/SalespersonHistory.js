import React, { useState, useEffect } from 'react';

const SalespersonHistory = () => {
    const [selectedSalesperson, setSelectedSalesperson] = useState('');
    const [salesHistory, setSalesHistory] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState('');

    useEffect(() => {
        const fetchSalespeople = async () => {
            setLoading(true);
            try {
                const response = await fetch('http://localhost:8090/api/salespeople');
                if (!response.ok) {
                    throw new Error(`Failed to fetch salespeople: ${response.statusText}`);
                }

                const data = await response.json();
                setSalespeople(data.salespeople);
            } catch (error) {
                console.error('Error fetching salespeople:', error);
                setError('Failed to fetch salespeople.');
            } finally {
                setLoading(false);
            }
        };

        fetchSalespeople();
    }, []);

    useEffect(() => {
        const fetchSalesHistory = async () => {
            if (!selectedSalesperson) {
                setSalesHistory([]);
                return;
            }

            setLoading(true);
            try {
                const response = await fetch(`http://localhost:8090/api/sales/${selectedSalesperson}`);
                if (!response.ok) {
                    throw new Error(`Failed to fetch sales history: ${response.statusText}`);
                }

                const data = await response.json();
                setSalesHistory(data.salesHistory);
            } catch (error) {
                console.error('Error fetching sales history:', error);
                setError('Failed to fetch sales history.');
            } finally {
                setLoading(false);
            }
        };

        fetchSalesHistory();
    }, [selectedSalesperson]);

    const handleChange = (e) => {
        setSelectedSalesperson(e.target.value);
    };

    return (
        <div>
            <h2>Salesperson History</h2>
            <div>
                <select value={selectedSalesperson} onChange={handleChange}>
                    <option value="">Select a Salesperson</option>
                    {salespeople.map((sales) => (
                        <option key={sales.salesperson.id} value={sales.salesperson.id}>
                            {sales.salesperson.first_name} {sales.salesperson.last_name}
                        </option>
                    ))}
                </select>
            </div>
            {error && <p style={{ color: 'red' }}>{error}</p>}
            {loading ? <p>Loading...</p> : (
                <table className='table table-striped'>
                    <thead>
                        <tr>
                            <th>Salesperson</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {salesHistory.map((sales, index) => (
                            <tr key={index}>
                                <td>{sales.salesperson.id}</td>
                                <td>{sales.customer.first_name}</td>
                                <td>{sales.automobile.vin}</td>
                                <td>${sales.price.toFixed(2)}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            )}
        </div>
    );
};

export default SalespersonHistory;
