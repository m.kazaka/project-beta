import { useEffect, useState } from 'react';

function AppointmentList() {
    const [successMessage, setSuccessMessage] = useState(false);
    const [appointments, setAppointment] = useState([]);
    const [soldVins, setSoldVins] = useState([]);

    const getData = async () => {
        try {
            const responseAppointments = await fetch('http://localhost:8080/api/appointments/');
            const responseAutomobiles = await fetch('http://localhost:8100/api/automobiles/');

            if (responseAppointments.ok && responseAutomobiles.ok) {
                const dataAppointments = await responseAppointments.json();
                const dataAutomobiles = await responseAutomobiles.json();

                const pendingAppointments = dataAppointments.appointments.filter(appointment => appointment.status.status === 'pending');

                setAppointment(pendingAppointments);


                const soldVinsList = dataAutomobiles.autos.filter(automobile => automobile.sold).map(automobile => automobile.vin);
                setSoldVins(soldVinsList);

            } else {
                throw new Error('failed to fetch data');
            }
        } catch (error) {
            console.error('Error fetching appointment data', error);
        }
    };
    useEffect(() => {
        getData()
    }, [])

    const handleUpdateStatus = async (id, newStatus) => {
        try {
            const url = `http://localhost:8080/api/appointments/${id}/${newStatus.toLowerCase()}/`;
            const fetchConfig = {
                method: 'PUT',
                body: JSON.stringify({ status: newStatus }),
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(url, fetchConfig);

            if (response.ok) {
                getData();
                setSuccessMessage(`Appointment successfully ${newStatus}ed`)
                setTimeout(() => {
                    setSuccessMessage('');
                }, 3000);
            } else {
                throw new Error(`Failed to update status to ${newStatus}`);
            }
        } catch (error) {
            console.error(`Error updating status to ${newStatus}`, error);
        }
    };

    return (
        <>
            <div>
                <h1>Service Appointments</h1>
                <table className='table table-striped'>
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Is VIP?</th>
                            <th>Customer</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th>Appt. Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {appointments.map(appointment => {
                            const isVip = soldVins.includes(appointment.vin) ? 'Yes' : 'No';
                            const formattedDate = new Date(appointment.date_time).toLocaleDateString();
                            const formattedTime = new Date(appointment.date_time).toLocaleTimeString([], {
                                hour: '2-digit',
                                minute: '2-digit',
                            });
                            return (
                                <tr key={ appointment.id } >
                                    <td>{ appointment.vin.toUpperCase() }</td>
                                    <td>{ isVip }</td>
                                    <td>{ appointment.customer }</td>
                                    <td>{ formattedDate }</td>
                                    <td>{ formattedTime }</td>
                                    <td>{ appointment.technician.first_name } { appointment.technician.last_name }</td>
                                    <td>{ appointment.reason }</td>
                                    <td>
                                        { appointment.status.status === 'pending' && (
                                            <button className="btn btn-outline-success"
                                            onClick={() => handleUpdateStatus(appointment.id, 'Finish')}>
                                                Finish
                                            </button>
                                        )}
                                        { appointment.status.status === 'pending' && (
                                            <button className="btn btn-outline-danger"
                                            onClick={() => handleUpdateStatus(appointment.id, 'Cancel')}>
                                                Cancel
                                            </button>
                                        )}
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
                {successMessage && (
                    <div className="alert alert-success mb-0" id="success-message">
                        {successMessage}
                    </div>
                )}
            </div>
        </>
    );
}

export default AppointmentList;