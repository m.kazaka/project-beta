import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

const RecordSaleForm = () => {
  const [formData, setFormData] = useState({
    automobileId: '',
    salespersonId: '',
    customerId: '',
    price: ''
  });
  const navigate = useNavigate();
  const [sales, setSales] = useState([]);

  const handleSubmit = async (event) => {
    event.preventDefault();


    const saleToRecord = {
      automobile_vin: formData.automobileId,
      salesperson_id: formData.salespersonId,
      customer_id: parseInt(formData.customerId),
      price: parseFloat(formData.price)
    };

    const url = 'http://localhost:8090/api/sales/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(saleToRecord),
      headers: {
        'Content-Type': 'application/json',

      },
    };

    try {
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        const newSale = await response.json();
        setSales([...sales, newSale]);
        setFormData({
          automobileId: '',
          salespersonId: '',
          customerId: '',
          price: ''
        });
        navigate('/sales/list');
      } else {
        const errorText = await response.text();
        throw new Error(`Failed to record sale: ${errorText}`);
      }
    } catch (error) {
      console.error('Error:', error);

    }
  };

  const handleFormChange = (e) => {
    const inputName = e.target.name;
    const value = e.target.value;

    setFormData((prevState) => ({
      ...prevState,
      [inputName]: value,
    }));
  };

  return (
    <>
      <div>
        <h1>Record Sale</h1>
        <form onSubmit={handleSubmit} id="record-sale-form">
          <div className="mb-3">
            <input onChange={handleFormChange}
              value={formData.automobileId}
              placeholder="Automobile VIN..."
              required type="text"
              name="automobileId"
              autoComplete="off"
              className="form-control" />
          </div>
          <div className="mb-3">
            <input onChange={handleFormChange}
              value={formData.salespersonId}
              placeholder="Salesperson ID..."
              required type="text"
              name="salespersonId"
              autoComplete="off"
              className="form-control" />
          </div>
          <div className="mb-3">
            <input onChange={handleFormChange}
              value={formData.customerId}
              placeholder="Customer ID..."
              required type="text"
              name="customerId"
              autoComplete="off"
              className="form-control" />
          </div>
          <div className="mb-3">
            <input onChange={handleFormChange}
              value={formData.price}
              placeholder="Price..."
              required type="number"
              step="0.01"
              name="price"
              autoComplete="off"
              className="form-control" />
          </div>
          <button type="submit" className="btn btn-primary" id="submit-sale">
            <span>Record Sale</span>
          </button>
        </form>

        <ul>
          {sales.map((sale, index) => (
            <li key={index}>
              VIN: {sale.automobile.vin}, Salesperson ID: {sale.salesperson_id}, Customer ID: {sale.customer_id}, Price: ${sale.price}
            </li>
          ))}
        </ul>
      </div>
    </>
  );
};

export default RecordSaleForm;
