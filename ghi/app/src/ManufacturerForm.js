import React, {  useState } from "react";

function ManufacturerForm() {
    const [formSuccess, setFormSuccess] = useState(false);
    const [formData, setFormData] = useState ({
        name: '',
    })
    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setFormData({
                name: '',
            });
            setFormSuccess(true);
            console.log(setFormData)
        }
    }
    const handleFormChange = (e) => {
        const inputName = e.target.name;
        const value = e.target.value;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (formSuccess) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none'
    }
    return (
        <>
            <div>
                <h1>Create a Manufacturer</h1>
                <form onSubmit={handleSubmit} id="add-manufacturer-form">
                <div className="mb-3">
                    <label htmlFor="name">Manufacturer</label>
                    <input onChange={handleFormChange}
                    value={formData.name}
                    placeholder="Manufacturers name..."
                    required type="text"
                    name="name"
                    id="name"
                    autoComplete="off"
                    className="form-control" />
                </div>
                <button className="btn btn-primary"
                id="style-2"
                data-replace="" ><span>Create</span></button>
                </form>
                <div className={messageClasses} id="success-message">
                Manufacturer created successfully!
            </div>
            </div>
        </>
    )

}

export default ManufacturerForm;