import { useEffect, useState } from 'react';

function ServiceHistory() {
    const [appointments, setAppointment] = useState([]);
    const [soldVins, setSoldVins] = useState([]);
    const [searchTerm, setSearchTerm] = useState('');

    const getData = async () => {
        try {
            const responseAppointments = await fetch('http://localhost:8080/api/appointments/');
            const responseAutomobiles = await fetch('http://localhost:8100/api/automobiles/');


            if (responseAppointments.ok && responseAutomobiles.ok) {
                const dataAppointments = await responseAppointments.json();
                const dataAutomobiles = await responseAutomobiles.json();

                setAppointment(dataAppointments.appointments);

                const soldVinsList = dataAutomobiles.autos.filter(automobile => automobile.sold).map(automobile => automobile.vin);
                setSoldVins(soldVinsList);

            } else {
                throw new Error('failed to fetch data');
            }
        } catch (error) {
            console.error('Error fetching appointment data', error);
        }
    };
    useEffect(() => {
        getData()
    }, [])

    const filteredAppointments = appointments.filter(appointment => appointment.vin.toLowerCase().includes(searchTerm.toLowerCase()));

    const handleSearchChange = (event) => {
        setSearchTerm(event.target.value);
    };

    return (
        <div>
            <h1>Service History</h1>
            <div className='input-group mb-3'>
                <input className='form-control'
                placeholder='Search by VIN...'
                type='text'
                id='search'
                value={searchTerm}
                onChange={handleSearchChange}
                style={{ textTransform: 'uppercase' }} />
            </div>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is Vip?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredAppointments.map(appointment => {
                        const isVip = soldVins.includes(appointment.vin) ? 'Yes' : 'No';
                        const formattedDate = new Date(appointment.date_time).toLocaleDateString();
                        const formattedTime = new Date(appointment.date_time).toLocaleTimeString([], {
                            hour: '2-digit',
                            minute: '2-digit',
                        });
                        return (
                            <tr key={ appointment.id }>
                                <td>{ appointment.vin.toUpperCase() }</td>
                                <td>{ isVip }</td>
                                <td>{ appointment.customer }</td>
                                <td>{ formattedDate }</td>
                                <td>{ formattedTime }</td>
                                <td>{ appointment.technician.first_name } { appointment.technician.last_name }</td>
                                <td>{ appointment.reason }</td>
                                <td>{ appointment.status.status === 'pending' ? 'created' : appointment.status.status }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}
export default ServiceHistory;