import React, {  useEffect, useState } from "react";

function TechnicianForm() {
    const [formSuccess, setFormSuccess] = useState(false);
    const [formData, setFormData] = useState ({
        first_name: '',
        last_name: '',
        employee_id: '',
    });

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type' : 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setFormData({
                employee_id: '',
                first_name: '',
                last_name: '',
            });
            setFormSuccess(true);
            console.log(setFormData)
        }
    };

    const handleFormChange = (e) => {
        const inputName = e.target.name;
        const value = e.target.value;

        setFormData((prevState) => ({
            ...prevState,
            [inputName]: value,
          }));
    };
        useEffect(() => {

            if (formData.first_name && formData.last_name) {
                const username = `${formData.first_name[0].toLowerCase()}${formData.last_name.slice(0, formData.last_name.length).toLowerCase()}`;
                console.log('New employee_id:', username);

            setFormData((prevState) => ({
                ...prevState,
                employee_id: username,
              }));
            }
          }, [formData.first_name, formData.last_name]);

        let messageClasses = 'alert alert-success d-none mb-0';
        let formClasses = '';
        if (formSuccess) {
            messageClasses = 'alert alert-success mb-0';
            formClasses = 'd-none'
        }
    return (
        <>
        <div>
            <h1>Add a Technician</h1>
            <form onSubmit={handleSubmit} id="add-technician-form">
                <div className="mb-3">
                    <input onChange={handleFormChange}
                    value={formData.first_name}
                    placeholder="First name..."
                    required type="text"
                    name="first_name"
                    id="first_name"
                    autoComplete="off"
                    className="form-control" />
                </div>
                <div className="mb-3">
                    <input onChange={handleFormChange}
                    value={formData.last_name}
                    placeholder="Last name..."
                    required type="text"
                    name="last_name"
                    id="last_name"
                    autoComplete="off"
                    className="form-control" />
                </div>
                <div className="mb-3">
                    <input onChange={handleFormChange}
                    value={formData.employee_id}
                    placeholder="Employee ID..."
                    required type="text"
                    name="employee_id"
                    id="employee_id"
                    autoComplete="off"
                    className="form-control" />
                </div>
                <button className="btn btn-primary"
                id="style-2"
                data-replace="" >Create</button>
            </form>
            <div className={messageClasses} id="success-message">
                Technician created successfully!
            </div>
        </div>
        </>
    );
}
export default TechnicianForm;
