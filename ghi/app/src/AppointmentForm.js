import { useEffect, useState } from 'react';

function AppointmentForm(){
    const [formSuccess, setFormSuccess] = useState(false);
    const [technicians, setTechnicians] = useState([])
    const [formData, setFormData] = useState ({
        customer: '',
        vin: '',
        date_time: '',
        technician: '',
        reason: '',
        status: 'pending',
    })

    const getData = async () => {
        const url = 'http://localhost:8080/api/technicians/'
        const response = await fetch(url);

        if(response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    }
    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = 'http://localhost:8080/api/appointments/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setFormData({
                customer: '',
                vin: '',
                date_time: '',
                technician: '',
                reason: '',
                status: 'pending',
            });
            setFormSuccess(true);
            console.log(setFormData);
        }
    }
    const handleFormChange = (e) => {
        const inputName = e.target.name;
        const value = e.target.value;
        setFormData({
            ...formData,
            [inputName]: value,
        });
    }
    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (formSuccess) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none'
    }
    return (
        <>
        <div>
            <h1>Create a Service Appointment</h1>
            <form onSubmit={handleSubmit} id="create-appointment-form">
            <div className="mb-3">
                    <label htmlFor='vin'>Automobile VIN</label>
                    <input onChange={handleFormChange}
                    value={formData.vin}
                    required type="text"
                    name="vin"
                    id='vin'
                    className="form-control"
                    style={{ textTransform: 'uppercase' }} />
                </div>
            <div className="mb-3">
            <label htmlFor='customer'>Customer</label>
                    <input onChange={handleFormChange}
                    value={formData.customer}
                    required type="text"
                    name="customer"
                    id='customer'
                    className="form-control" />
                </div>
            <div className="mb-3">
            <label htmlFor="date_time">Date</label>
                    <input onChange={handleFormChange}
                    value={formData.date_time}
                    required type="datetime-local"
                    name="date_time"
                    id='date_time'
                    className="form-control" />
                </div>
            <div className="mb-3">
            <label htmlFor='reason'>Reason</label>
                    <input onChange={handleFormChange}
                    value={formData.reason}
                    required type="text"
                    name="reason"
                    id='reason'
                    className="form-control" />
                </div>
                <div className="mb-3">
              <label htmlFor='technician' >Technician</label>
              <select onChange={handleFormChange}
              value={formData.technician}
              required name="technician"
              id="technician"
              className="form-select" >
              <option value="">Choose a technician...</option>
                {technicians.map(technician => {
                  return (
                    <option key={technician.id} value={technician.id}>{technician.employee_id}</option>
                  );
                })}
            </select>
            </div>
            <button className="btn btn-primary"
            id="style-2"
            data-replace="" >Create</button>
            </form>
            <div className={messageClasses} id="success-message">
                Appointment created successfully!
            </div>
        </div>
        </>
    )
}

export default AppointmentForm