import React, {  useEffect, useState } from "react";

function AutomobilesList () {
    const[autos, setAutos] = useState([])

    const getData = async () => {
        try {
            const response = await fetch('http://localhost:8100/api/automobiles/');
            if (response.ok) {
                const data = await response.json();
                setAutos(data.autos);
            } else {
                throw new Error('Failed to fetch data');
            }
        } catch (error) {
            console.error('error fetching vehicle model data', error);
        }
    };
    useEffect(() =>{
        getData()
    }, [])
    return (
        <>
        <div>
            <h1>Automobiles</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {autos.map(auto => {
                        return (
                            <tr key={ auto.id }>
                                <td>{ auto.vin.toUpperCase() }</td>
                                <td>{ auto.color }</td>
                                <td>{ auto.year }</td>
                                <td>{ auto.model.name }</td>
                                <td>{ auto.sold ? 'Yes' : 'No' }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
        </>
    )
}

export default AutomobilesList;