import React, {  useEffect, useState } from "react";

function ModelsList () {
    const[models, setModels] = useState([])

    const getData = async () => {
        try {
            const response = await fetch('http://localhost:8100/api/models/');
            if (response.ok) {
                const data = await response.json();
                setModels(data.models);
            } else {
                throw new Error('Failed to fetch data');
            }
        } catch (error) {
            console.error('error fetching vehicle model data', error);
        }
    };
    useEffect(() =>{
        getData()
    }, [])
    return (
        <>
        <div>
            <h1>Vehicle Models</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => {
                        return (
                            <tr key={ model.id }>
                                <td>{ model.name }</td>
                                <td>{ model.manufacturer.name }</td>
                                <td>
                                    <img
                                    src={ model.picture_url }
                                    alt={`${model.name}`}
                                    style={{ maxWidth: '100px', maxHeight: '100px'}}
                                    />
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
        </>
    )
}

export default ModelsList;