import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

const AddCustomerForm = () => {
  const [formData, setFormData] = useState({
    first_name: '',
    last_name: '',
    customer_id: '',
    phone_number: '',
    address: ''
  });
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();
    setError('');
    setLoading(true);


    if (!formData.first_name.trim() || !formData.last_name.trim()) {
      setError('First name and last name are required.');
      setLoading(false);
      return;
    }

    const url = formData.customer_id ? `http://localhost:8090/api/customers/${formData.customer_id}` : 'http://localhost:8090/api/customers/';
    const method = formData.customer_id ? 'PUT' : 'POST';

    try {
      const fetchConfig = {
        method: method,
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          first_name: formData.first_name,
          last_name: formData.last_name,
          phone_number: formData.phone_number,
          address: formData.address
        }),
      };

      const response = await fetch(url, fetchConfig);
      if (!response.ok) throw new Error('Failed to create/update customer');


      setFormData({
        first_name: '',
        last_name: '',
        customer_id: '',
        phone_number: '',
        address: ''
      });

      navigate('/customers/list');
    } catch (error) {
      console.error('Error:', error);
      setError('Failed to create/update customer. Please try again.');
    } finally {
      setLoading(false);
    }
  };

  const handleFormChange = (e) => {
    const { name, value } = e.target;
    setFormData(prevState => ({
      ...prevState,
      [name]: value,
    }));
  };

  return (
    <div>
      <h2>{formData.customer_id ? 'Update Customer' : 'Add Customer'}</h2>
      {error && <p style={{ color: 'red' }}>{error}</p>}
      <form onSubmit={handleSubmit}>
        <label>First Name:</label>
        <input type="text" name="first_name" value={formData.first_name} onChange={handleFormChange} required />

        <label>Last Name:</label>
        <input type="text" name="last_name" value={formData.last_name} onChange={handleFormChange} required />

        <label>Phone Number:</label>
        <input type="text" name="phone_number" value={formData.phone_number} onChange={handleFormChange} />

        <label>Address:</label>
        <input type="text" name="address" value={formData.address} onChange={handleFormChange} />

        <button type="submit" disabled={loading}>
          {loading ? 'Processing...' : formData.customer_id ? 'Update Customer' : 'Add Customer'}
        </button>
      </form>
    </div>
  );
};

export default AddCustomerForm;
