import React, {  useEffect, useState } from "react";

function AutomobilesForm () {
    const [formSuccess, setFormSuccess] = useState(false);
    const [models, setModels] = useState([])
    const [formData, setFormData] = useState ({
        color: '',
        year: '',
        vin: '',
        model: '',
        sold: false,
    })

    const getData = async () => {
        const url = 'http://localhost:8100/api/models/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setModels(data.models)
        }
    }
    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setFormData({
                color: '',
                year: '',
                vin: '',
                model: '',
                sold: false,
            });
            setFormSuccess(true);
            console.log(setFormData)
        }
    }
    const handleFormChange = (e) => {
        const inputName = e.target.name;
        const value = e.target.value;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }
    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (formSuccess) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none'
    }
    return (
        <>
            <div>
                <h1>Add an Automobile to Inventory</h1>
                <form onSubmit={handleSubmit} id="add-automobile-form">
                <div className="mb-3">
                    <input onChange={handleFormChange}
                    value={formData.color}
                    placeholder="Color..."
                    required type="text"
                    name="color"
                    id="color"
                    autoComplete="off"
                    className="form-control" />
                </div>
                <div className="mb-3">
                    <input onChange={handleFormChange}
                    value={formData.year}
                    placeholder="Year..."
                    required type="text"
                    name="year"
                    id="year"
                    autoComplete="off"
                    className="form-control" />
                </div>
                <div className="mb-3">
                    <input onChange={handleFormChange}
                    value={formData.vin}
                    placeholder="VIN..."
                    required type="text"
                    name="vin"
                    id="vin"
                    autoComplete="off"
                    className="form-control"
                    style={{ textTransform: 'uppercase' }} />
                </div>
                <div className="mb-3">
                    <select onChange={handleFormChange}
                    value={formData.model}
                    placeholder="Choose a model..."
                    required type="text"
                    name="model"
                    id="model"
                    autoComplete="off"
                    className="form-control" >
                    <option value="">Choose a Model</option>
                    {models.map(model => {
                        return (
                            <option key={model.id} value={model.id}>
                                {model.name}
                            </option>
                        );
                    })}
                    </select>
                </div>
                <button className="btn btn-primary"
                id="style-2"
                data-replace=""
                type="submit" >Create</button>
                </form>
                <div className={messageClasses} id="success-message">
                    Automobile created successfully!
            </div>
            </div>
        </>
    )
}

export default AutomobilesForm;