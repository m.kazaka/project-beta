import React, { useEffect, useState } from 'react';

const ListSales = () => {
  const [sales, setSales] = useState([]);

  const fetchSalesData = async () => {
    try {
      const response = await fetch('http://localhost:8090/api/sales/');
      if (!response.ok) {
        throw new Error(`Failed to fetch sales: ${response.statusText}`);
      }
      const data = await response.json();
      setSales(data.sales);
    } catch (error) {
      console.error('Error fetching sales:', error);
    }
  };

  useEffect(() => {
    fetchSalesData();
  }, []);

  return (
    <div>
      <h2>List of Sales</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Sale ID</th>
            <th>Automobile VIN</th>
            <th>Salesperson ID</th>
            <th>Customer ID</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map((sale) => (
            <tr key={sale.id}>
              <td>{sale.id}</td>
              <td>{sale.automobile.vin}</td>
              <td>{sale.salesperson.employee_id} </td>
              <td>{sale.customer.first_name} {sale.customer.last_name}</td>
              <td>{sale.price}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ListSales;
