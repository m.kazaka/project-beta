import React from 'react';
import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse flex-column ml-lg-0 ml-3" id="navbarSupportedContent">

          <ul className="navbar-nav">
          <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturer/add">Create a Manufacturer</NavLink>
            </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturer/list">Manufacturers</NavLink>
            </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/models/new">Create a Model</NavLink>
            </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/models/list">Models</NavLink>
            </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles/new">Create an Automobiles</NavLink>
            </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles/list">Automobiles</NavLink>
            </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/appointments/history">Service History</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/technician/add">Add a Technician</NavLink>
            </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/technician/list">Technicians</NavLink>
            </li>
            </ul>
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item">
              <NavLink className="nav-link" to="/appointments/new">Create a Service Appointment</NavLink>
            </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/appointments/list">Service Appointments</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link"  to="/customers/add">Add Customer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link"  to="/customers/list">Customers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link"  to="/salespeople/add">Add Salesperson</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link"  to="/salespeople/list">Salespeople</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link"  to="/sales/list">Sales</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link"  to="/sales/add">Add Sale</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/salespeople/history">Salesperson History</NavLink>
            </li>

          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
