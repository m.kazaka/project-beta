import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Nav from './Nav';
import AddSalespersonForm from './AddSalespersonForm';
import ListSalespeople from './ListSalespeople';
import AddCustomerForm from './AddCustomerForm';
import ListCustomers from './CustomerList';
import RecordSaleForm from './RecordSaleForm';
import ListSales from './ListSales';
import SalespersonHistory from './SalespersonHistory';
import MainPage from './MainPage';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import ServiceHistory from './ServiceHistory';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturerList';
import ModelsForm from './ModelsForm';
import ModelsList from './ModelsList';
import AutomobilesForm from './AutomobilesForm';
import AutomobilesList from './AutomobilesList';

function App() {
  return (
    <Router>
      <div className="container">
        <Nav />
        <Routes>
        <Route path="/" element={<MainPage />} />

          <Route path="/salespeople">
            <Route path="list" element={<ListSalespeople />} />
            <Route path="add" element={<AddSalespersonForm />} />
            <Route path="history" element={<SalespersonHistory />} />
          </Route>
          <Route path="/customers">
            <Route path="add" element={<AddCustomerForm />} />
            <Route path="list" element={<ListCustomers />} />
          </Route>
          <Route path="/sales" >
            <Route path="add" element={<RecordSaleForm />} />
            <Route path="list" element={<ListSales />} />
          </Route>
          <Route path='/technician'>
            <Route path="add" element={<TechnicianForm />} />
            <Route path="list" element={<TechnicianList />} />
          </Route>
          <Route path='/appointments'>
            <Route path="new" element={<AppointmentForm />} />
            <Route path='list' element={<AppointmentList />} />
            <Route path='history' element={<ServiceHistory />} />
          </Route>
          <Route path='/manufacturer' >
            <Route path="add" element={<ManufacturerForm />} />
            <Route path='list' element={<ManufacturerList />} />
          </Route>
          <Route path='/models'>
            <Route path="new" element={<ModelsForm />} />
            <Route path="list" element={<ModelsList />} />
          </Route>
          <Route path='/automobiles'>
            <Route path="new" element={<AutomobilesForm />} />
            <Route path="list" element={<AutomobilesList />} />
          </Route>
        </Routes>
      </div>
    </Router>
  );
};

export default App;
