import React, {  useEffect, useState } from "react";

function ModelsForm () {
    const [formSuccess, setFormSuccess] = useState(false);
    const [manufacturers, setManufacturers] = useState([])
    const [formData, setFormData] = useState ({
        name: '',
        picture_url: '',
        manufacturer_id: '',
    })

    const getData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers)
        }
    }
    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setFormData({
                name: '',
                picture_url: '',
                manufacturer_id: '',
            });
            setFormSuccess(true);
            console.log(setFormData)
        }
    }
    const handleFormChange = (e) => {
        const inputName = e.target.name;
        const value = e.target.value;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }
    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (formSuccess) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none'
    }
    return (
        <>
            <div>
                <h1>Create a Vehicle Model</h1>
                <form onSubmit={handleSubmit} id="add-models-form">
                <div className="mb-3">
                    <label htmlFor="name">Name</label>
                    <input onChange={handleFormChange}
                    value={formData.name}
                    placeholder="ex; Accord, Model S, Tacoma..."
                    required type="text"
                    name="name"
                    id="name"
                    autoComplete="off"
                    className="form-control" />
                </div>
                <div className="mb-3">
                    <label htmlFor="picture_url">Picture URL</label>
                    <input onChange={handleFormChange}
                    value={formData.picture_url}
                    placeholder="Paste image url..."
                    required type="text"
                    name="picture_url"
                    id="picture_url"
                    autoComplete="off"
                    className="form-control" />
                </div>
                <div className="mb-3">
                    <label htmlFor="manufacturer_id">Manufacturer</label>
                    <select onChange={handleFormChange}
                    value={formData.manufacturer_id}
                    placeholder="Manufacturers name..."
                    required type="text"
                    name="manufacturer_id"
                    id="manufacturer_id"
                    autoComplete="off"
                    className="form-control" >
                    <option value="">Choose a Manufacturer</option>
                    {manufacturers.map(manufacturer => {
                        return (
                            <option key={manufacturer.href} value={manufacturer.id}>
                                {manufacturer.name}
                            </option>
                        );
                    })}
                    </select>
                </div>
                <button className="btn btn-primary"
                id="style-2"
                data-replace=""
                type="submit" >Create</button>
                </form>
                <div className={messageClasses} id="success-message">
                    Vehicle Model created successfully!
            </div>
            </div>
        </>
    )
}

export default ModelsForm;