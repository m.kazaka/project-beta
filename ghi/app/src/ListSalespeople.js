import React, { useEffect, useState } from 'react';

function ListSalespeople() {
  const [salespeople, setSalespeople] = useState([]);

  const fetchSalespeople = async () => {
    try {
      const response = await fetch('http://localhost:8090/api/salespeople/');
      if (!response.ok) {
        throw new Error(`Failed to fetch salespeople: ${response.statusText}`);
      }
      const data = await response.json();
      setSalespeople(data.salespeople);
    } catch (error) {
      console.error('Error fetching salespeople:', error);
    }
  };

  useEffect(() => {
    fetchSalespeople();
  }, []);

  return (
    <div>
      <h2>List of Salespeople</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
        </thead>
        <tbody>
          {salespeople.map((salesperson) => (
            <tr key={salesperson.employee_id}>
              <td>{salesperson.employee_id}</td>
              <td>{salesperson.first_name}</td>
              <td>{salesperson.last_name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ListSalespeople;
